# Calculo Numerico - 2019 de la Universidad del Comahue. Centro Regional Bariloche

## Profesorado y Licenciatura en Matematicas
http://crubweb.uncoma.edu.ar/

### Dr. René Cejas Bolecek
### rene.bolecek@gmail.com

Material de teoría, prácticas y algunos de los módulos en python utilizados en la materia. 

## Calendario 2018 2do cuatrimestre:

12/08: U1. Sistema posicional. Conversión entre sistemas numéricos. Representación de números enteros: punto fijo. 

15/08: U1. Redondeo y truncamiento. Error absoluto y relativo.

Estimación fecha primer parcial ( Incluye todos los temas anteriores): 23/09

Temas siguientes clases a confirmar...

licence: MIT. http://opensource.org/licenses/MIT 